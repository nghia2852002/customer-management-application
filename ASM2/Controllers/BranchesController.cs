﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

[Route("api/branches")]
[ApiController]
public class BranchesController : ControllerBase
{
    private List<Branch> branches;
    private readonly string jsonFilePath = "branches.json"; // Thay đổi thành đường dẫn tệp JSON thực tế của bạn

    public BranchesController()
    {
        // Đọc dữ liệu từ tệp JSON và lưu vào biến branches
        if (System.IO.File.Exists(jsonFilePath))
        {
            var json = System.IO.File.ReadAllText(jsonFilePath);
            branches = JsonSerializer.Deserialize<List<Branch>>(json);
        }
        else
        {
            branches = new List<Branch>();
        }
    }

    // API để lấy toàn bộ danh sách chi nhánh
    [HttpGet("get-all")]
    public ActionResult<IEnumerable<Branch>> GetAllBranches()
    {
        return Ok(branches);
    }

    // API để thêm một chi nhánh mới
    [HttpPost("add")]
    public ActionResult<Branch> AddBranch([FromBody] Branch branch)
    {
        if (branch == null)
        {
            return BadRequest("Invalid data.");
        }

        // Gán mã chi nhánh tự động (thay thế bằng cách xác định mã chi nhánh duy nhất)
        branch.BranchId = branches.Count + 1;

        branches.Add(branch);

        // Lưu danh sách chi nhánh vào tệp JSON
        var json = JsonSerializer.Serialize(branches);
        System.IO.File.WriteAllText(jsonFilePath, json);

        return CreatedAtAction(nameof(GetAllBranches), new { id = branch.BranchId }, branch);
    }

    // API để cập nhật thông tin chi nhánh
    [HttpPut("update/{id}")]
    public ActionResult<Branch> UpdateBranch(int id, [FromBody] Branch updatedBranch)
    {
        if (updatedBranch == null)
        {
            return BadRequest("Invalid data.");
        }

        var existingBranch = branches.FirstOrDefault(b => b.BranchId == id);
        if (existingBranch == null)
        {
            return NotFound();
        }

        existingBranch.Name = updatedBranch.Name;
        existingBranch.Address = updatedBranch.Address;
        existingBranch.City = updatedBranch.City;
        existingBranch.State = updatedBranch.State;
        existingBranch.ZipCode = updatedBranch.ZipCode;

        // Lưu danh sách chi nhánh vào tệp JSON
        var json = JsonSerializer.Serialize(branches);
        System.IO.File.WriteAllText(jsonFilePath, json);

        return Ok(existingBranch);
    }

    // API để xóa một chi nhánh
    [HttpDelete("delete/{id}")]
    public ActionResult DeleteBranch(int id)
    {
        var existingBranch = branches.FirstOrDefault(b => b.BranchId == id);
        if (existingBranch == null)
        {
            return NotFound();
        }

        branches.Remove(existingBranch);

        // Lưu danh sách chi nhánh vào tệp JSON
        var json = JsonSerializer.Serialize(branches);
        System.IO.File.WriteAllText(jsonFilePath, json);

        return NoContent();
    }
}
